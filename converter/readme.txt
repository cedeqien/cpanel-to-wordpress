Things to do once it's converted:
 - wp term recount category
 - wp term recount post_tag
 - to fix perma-links : admin -> settings -> permalinks -> save
 - install the regenerate thumbnails plugin
 - admin -> tools -> regenerate thumbnails to regenerate the thumbnails.