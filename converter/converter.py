import MySQLdb
from slugify import slugify
from datetime import datetime, timedelta


def main():
    cpanel = MySQLdb.connect(host='127.0.0.1', user='root', database='mystery')
    converted = MySQLdb.connect(host='127.0.0.1', user='root', database='converted_mystery')
    # categories(cpanel, converted)
    # authors(cpanel, converted)
    # tags(cpanel, converted)
    articles(cpanel, converted)


def authors(cpanel, converted):
    p_cursor = cpanel.cursor()
    c_cursor = converted.cursor()
    p_cursor.execute('SELECT * FROM reviewer;')
    results = p_cursor.fetchall()
    for result in results:
        rv_id = result[0]
        rv_fname = result[1]
        rv_lname = result[2]
        display_name = rv_fname + ' ' + rv_lname
        rv_email = result[3]
        rv_url = result[4]
        try:
            c_cursor.execute("INSERT INTO wp_users (ID, user_login, user_pass, user_nicename, user_email, user_url,"
                             " user_registered, user_activation_key, user_status, display_name) VALUES (%s, %s, '', %s,"
                             "%s, %s, NOW(), '', 0, %s)", (rv_id, rv_fname, rv_fname, rv_email, rv_url, display_name))
            print('Authors have been converted.')
            converted.commit()
        except MySQLdb.Error as e:
            print(e)


def tags(cpanel, converted):
    p_cursor = cpanel.cursor()
    c_cursor = converted.cursor()
    p_cursor.execute('SELECT * FROM talent;')
    results = p_cursor.fetchall()
    for result in results:
        t_id = result[0]
        t_fname = result[1]
        t_lname = result[2]
        t_gname = result[3]
        t_email = result[4]
        t_url = result[5]
        t_bio = result[6]
        t_dob = result[7]
        t_dod = result[8]
        if t_fname != '':
            name = t_fname + ' ' + t_lname
        else:
            name = t_gname
        if t_gname != '000001 - Do not delete!':
            try:
                c_cursor.execute("INSERT INTO wp_terms (term_id, name, slug, term_group) VALUES (%s, %s, %s, 0)",
                                 (t_id + 100, name, slugify(name)))
                c_cursor.execute(
                    "INSERT INTO wp_term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, parent, count) "
                    "VALUES (%s, %s, 'post_tag', '', 0, 0)", (t_id + 100, t_id + 100))
                converted.commit()
                print('Tags have been converted.')

            except MySQLdb.Error as e:
                print(e)


def articles(cpanel, converted):
    p_cursor = cpanel.cursor()
    c_cursor = converted.cursor()
    p_cursor.execute('SELECT * FROM review;')
    results = p_cursor.fetchall()
    for result in results:
        r_id = result[0]  # ID
        r_title = result[1]
        r_talent_id = result[2]  # check into tags for this
        r_reviewer_id = result[3]  # post_author
        r_pub_class = result[4]
        r_type_id = result[5]
        c_cursor.execute("SELECT name from wp_terms where term_id IN (%s)", [r_type_id])
        category = c_cursor.fetchone()
        category_name = category[0]
        r_image = result[6]
        r_image_alt = result[7]
        r_image_link = result[8]
        r_pub_details = result[9]
        r_synopsis = result[10]
        r_text = result[11]  # post_content
        r_recommended = result[12]
        r_status = result[13]
        r_frontpage_eligible = result[14]
        r_gift_eligible = result[15]
        r_video_url = result[16]
        r_video_type = result[17]
        r_video_show = result[18]
        date = datetime.today() + timedelta(seconds=r_id)
        if 'http://www.amazon' in r_image_link:
            amazon_product = r_image_link.split('/')
            if 'http://www.amazon.com/exec/obidos/ASIN/' in r_image_link:
                id_amazon = amazon_product[6]
            elif 'http://www.amazon.com/gp/product/' in r_image_link:
                id_amazon = amazon_product[5]
            elif 'http://www.amazon.com/exec/obidos/tg/detail' in r_image_link:
                id_amazon = amazon_product[8]
            elif 'http://www.amazon.ca/gp/product/' in r_image_link:
                id_amazon = amazon_product[5]
            amazon_iframe = '<a target="_blank"  class="lien-amazon" ' \
                            'href="https://www.amazon.com/gp/product/' + id_amazon + '/ref=as_li_tl?ie=UTF8' \
                                                                                     '&camp=1789&creative=9325&creativeASIN=' + id_amazon + '&linkCode=as2&tag=geekstreet&linkId=1403f1548eb90d22255c7d1c86f5b0d0">' \
                                                                                                                                            '<img src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=' + id_amazon + \
                            '&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL160_&tag=geekstreet" ></a>' \
                            '<img src="//ir-na.amazon-adsystem.com/e/ir?t=geekstreet&l=am2&o=1&a=' + id_amazon + '" ' \
                                                                                                                 'width="1" height="1" alt="" style="margin:0px !important;" />'
            content_with_amazon = amazon_iframe + '<br>Support us click the picture and BUY at Amazon<br>' + r_pub_details + '<br>' + r_text + '<br>'
        else:
            content_with_amazon = r_pub_details + '<br>' + r_text + '<br>'
        if r_video_url != '':
            content_with_amazon = content_with_amazon + '<iframe width="560" height="315" src="https://www.youtube.com/embed/' + \
                                  r_video_url[
                                  31:42] + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
        meta_value_image = "2018/03/" + r_image
        guid = "http://127.0.0.1/converted_mystery/?p=" + str(
            r_id)  # not sure if this will stay this way on a prod server.
        guid_image = "http://127.0.0.1/converted_mystery/wp-content/uploads/2018/03/" + r_image  # not sure here either
        # make the post first
        c_cursor.execute("INSERT INTO wp_posts (ID, post_author, post_date, post_date_gmt, post_content, "
                         "post_title, post_excerpt, post_status, comment_status, ping_status, post_password, "
                         "post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent,"
                         "guid, menu_order, post_type, post_mime_type, comment_count) VALUES (%s, %s, "
                         "NOW(), NOW(), %s, %s, %s, 'publish', 'closed', 'closed',"
                         "'', %s, '', '', %s, %s, '', 0, %s, 0, 'post', '', 0)",
                         (r_id, r_reviewer_id, content_with_amazon, r_title, r_synopsis, slugify(r_title), date, date,
                          guid))
        # let's put all these posts in their categories
        c_cursor.execute("INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES"
                         " (%s, %s, 0)", (r_id, r_type_id))
        # and add tags to them
        if r_talent_id != 1:
            c_cursor.execute("INSERT INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES"
                             " (%s, %s, 0)", (r_id, r_talent_id + 100))
        # now that's done, we need to add the pictures to those posts.
        if not 'http://www.amazon.com/' in r_image_link:
            c_cursor.execute("INSERT INTO wp_posts (ID, post_author, post_date, post_date_gmt, post_content, "
                             "post_title, post_excerpt, post_status, comment_status, ping_status, post_password, "
                             "post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent,"
                             "guid, menu_order, post_type, post_mime_type, comment_count) VALUES (%s, %s, "
                             "NOW(), NOW(), '', %s, '', 'inherit', 'closed', 'closed',"
                             "'', %s, '', '', %s, %s, '', %s, %s, 0, 'attachment', 'image/jpeg', 0)",
                             (r_id + 10000, r_reviewer_id, r_image, r_image, date, date, r_id, guid_image))
            c_cursor.execute("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES "
                             "(%s, '_wp_attached_file', %s)", (r_id + 10000, meta_value_image))
            c_cursor.execute("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES "
                             "(%s, '_wp_attached_image_alt', %s)", (r_id + 10000, r_image_alt))
            c_cursor.execute("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES "
                             "(%s, '_thumbnail_id', %s)", (r_id, r_id + 10000))
            #  SEO stuff
        resume = ''
        for word in r_text.split()[:25]:
            resume = resume + word + ' '

        c_cursor.execute("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES"
                         "(%s, '_yoast_wpseo_metadesc', %s)", (r_id, resume))
        c_cursor.execute("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES"
                         "(%s, '_yoast_wpseo_focuskw', %s)", (r_id, category_name))
        c_cursor.execute("INSERT INTO wp_postmeta (post_id, meta_key, meta_value) VALUES"
                         "(%s, '_yoast_wpseo_focuskw_text_input', %s)", (r_id, category_name))

        converted.commit()
        print('Articles have been converted.')


def categories(cpanel, converted):
    p_cursor = cpanel.cursor()
    c_cursor = converted.cursor()
    p_cursor.execute('SELECT * FROM revtype;')
    results = p_cursor.fetchall()
    for result in results:
        id = result[0]
        rt_type = result[1]  # unused
        rt_linktext = result[2]
        try:
            c_cursor.execute("INSERT INTO wp_terms (term_id, name, slug, term_group) VALUES (%s, %s, %s, 0)",
                             (id, rt_linktext, slugify(rt_linktext)))
            c_cursor.execute(
                "INSERT INTO wp_term_taxonomy (term_taxonomy_id, term_id, taxonomy, description, parent, count) "
                "VALUES (%s, %s, 'category', '', 0, 0)", (id, id))
            converted.commit()
            print('Categories have been converted.')
        except MySQLdb.Error as e:
            print(e)


if __name__ == "__main__":
    main()
